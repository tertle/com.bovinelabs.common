// <copyright file="Empty.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Helpers
{
    /// <summary>
    /// Empty struct.
    /// </summary>
    public struct Empty
    {
    }
}