﻿// <copyright file="UnsafeUtilityExtensions.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Helpers
{
    using System;
    using Unity.Collections.LowLevel.Unsafe;

    /// <summary>
    /// The UnsafeUtilityExtensions.
    /// </summary>
    public static unsafe class UnsafeUtilityExtensions
    {
        /// <summary>
        /// MemCpy a destination offset.
        /// </summary>
        /// <param name="destination">The destination.</param>
        /// <param name="destinationOffset">The destination offset.</param>
        /// <param name="source">The source.</param>
        /// <param name="sourceOffset">The source offset.</param>
        /// <param name="elementSize">The element size.</param>
        /// <param name="length">The length.</param>
        public static void MemCpy(void* destination, int destinationOffset, void* source, int sourceOffset, int elementSize, long length)
        {
            destination = (byte*)((IntPtr)destination + (elementSize * destinationOffset));
            source = (byte*)((IntPtr)source + (elementSize * sourceOffset));
            UnsafeUtility.MemCpy(destination, source, length * elementSize);
        }
    }
}