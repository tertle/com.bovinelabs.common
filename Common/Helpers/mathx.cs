// <copyright file="mathx.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Helpers
{
    using System.Diagnostics.CodeAnalysis;
    using Unity.Mathematics;

    /// <summary>
    /// Math extensions that are not supported by the Unity.Mathematics library.
    /// </summary>
    [SuppressMessage("ReSharper", "SA1300", Justification = "Matching Unity.Mathematics style.")]
    public static class mathx
    {
        /// <summary>
        /// Returns the result of rounding a float value to digits.
        /// </summary>
        /// <param name="x">Value to round.</param>
        /// <param name="digits">Number of digits to round to.</param>
        /// <returns>The result.</returns>
        public static float round(float x, int digits)
        {
            return (float)System.Math.Round(x, digits);
        }

        /// <summary>
        /// Returns the result of rounding each component of a float2 vector value to digits.
        /// </summary>
        /// <param name="x">Value to round.</param>
        /// <param name="digits">Number of digits to round to.</param>
        /// <returns>The result.</returns>
        public static float2 round(float2 x, int digits)
        {
            return new float2(round(x.x, digits), round(x.y, digits));
        }

        /// <summary>
        /// Returns the result of rounding each component of a float3 vector value to digits.
        /// </summary>
        /// <param name="x">Value to round.</param>
        /// <param name="digits">Number of digits to round to.</param>
        /// <returns>The result.</returns>
        public static float3 round(float3 x, int digits)
        {
            return new float3(round(x.x, digits), round(x.y, digits), round(x.z, digits));
        }

        /// <summary>
        /// Returns the result of rounding each component of a float2 vector value to digits.
        /// </summary>
        /// <param name="x">Value to round.</param>
        /// <param name="digits">Number of digits to round to.</param>
        /// <returns>The result.</returns>
        public static float4 round(float4 x, int digits)
        {
            return new float4(round(x.x, digits), round(x.y, digits), round(x.z, digits), round(x.w, digits));
        }
    }
}