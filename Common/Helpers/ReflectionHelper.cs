// <copyright file="ReflectionHelper.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Unity.Entities;
    using UnityEngine;

    /// <summary>
    /// The ReflectionHelper.
    /// </summary>
    public static class ReflectionHelper
    {
        private static readonly Type[] AllTypes =
            AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes()).ToArray();

        /// <summary>
        /// Search all assemblies and create instances of all types that inherit from T.
        /// </summary>
        /// <typeparam name="T">The base type.</typeparam>
        /// <returns>List of instances.</returns>
        public static List<T> CreateAllOfType<T>()
        {
            var list = new List<T>();

            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (!TypeManager.IsAssemblyReferencingEntities(assembly))
                {
                    continue;
                }

                IEnumerable<Type> allTypes;

                try
                {
                    allTypes = assembly.GetTypes();
                }
                catch (ReflectionTypeLoadException e)
                {
                    allTypes = e.Types.Where(t => t != null);
                    Debug.LogWarning(
                        $"Failed loading assembly: {(assembly.IsDynamic ? assembly.ToString() : assembly.Location)}");
                }

                var types = allTypes.Where(t =>
                    typeof(T).IsAssignableFrom(t) &&
                    !t.IsAbstract &&
                    !t.ContainsGenericParameters);

                foreach (var type in types)
                {
                    try
                    {
                        var instance = (T)Activator.CreateInstance(type);
                        list.Add(instance);
                    }
                    catch (MissingMethodException)
                    {
                        Debug.LogError($"Installer {type} must be mentioned in a link.xml file, or annotated " +
                                       "with a [Preserve] attribute to prevent its constructor from being stripped.  " +
                                       "See https://docs.unity3d.com/Manual/ManagedCodeStripping.html for more information.");
                        throw;
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Gets a type from its name, exhaustively searching all assemblies if required.
        /// </summary>
        /// <param name="t">The name of the type.</param>
        /// <returns>The type, null if not found.</returns>
        public static Type GetTypeExhaustive(string t)
        {
            var type = Type.GetType(t);

            if (type == null)
            {
                type = AllTypes.FirstOrDefault(x => x.FullName == t);

                if (type == null)
                {
                    Debug.LogError($"Type {t} not found.");
                    return null;
                }

                Debug.LogWarning(
                    $"Type {t} was found but required an exhaustive full assembly search. For performance include the assembly name, '{type.FullName}, {type.Assembly.GetName().Name}'");
            }

            return type;
        }

        /// <summary>
        /// Checks if a type is a subclass of a genericType type.
        /// </summary>
        /// <param name="toCheck">The type to check.</param>
        /// <param name="genericType">The type to check against.</param>
        /// <returns>True if subclass.</returns>
        public static bool IsSubclassOfRawGeneric(this Type toCheck, Type genericType)
        {
            while (toCheck != null && toCheck != typeof(object))
            {
                var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (genericType == cur)
                {
                    return true;
                }

                toCheck = toCheck.BaseType;
            }

            return false;
        }

        /// <summary>
        /// Checks if an assembly is reference the nunit test framework.
        /// </summary>
        /// <param name="assembly">The assembly to test.</param>
        /// <returns>True if the framework is referenced.</returns>
        public static bool IsAssemblyReferencingTestFramework(Assembly assembly)
        {
            const string testFrameworkName = "nunit.framework";
            if (assembly.GetName().Name.Contains(testFrameworkName))
            {
                return true;
            }

            var referencedAssemblies = assembly.GetReferencedAssemblies();
            foreach (var referenced in referencedAssemblies)
            {
                if (referenced.Name.Contains(testFrameworkName))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Checks if an assembly is a Unity assembly.
        /// </summary>
        /// <param name="assembly">The assembly to test.</param>
        /// <returns>True if the assembly is a Unity assembly.</returns>
        public static bool IsUnityAssembly(Assembly assembly)
        {
            const string packageAssemblyName = "Unity.";
            const string unityEngineAssemblyName = "UnityEngine";

            var name = assembly.GetName().Name;

            if (name.StartsWith(packageAssemblyName) ||
                name.StartsWith(unityEngineAssemblyName))
            {
                return true;
            }

            return false;
        }
    }
}