// <copyright file="Intersections.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Helpers
{
    using Unity.Mathematics;

    /// <summary>
    /// Intersections.
    /// </summary>
    public static class Intersections
    {
        /// <summary>
        /// Checks if a circle and segment intersect.
        /// </summary>
        /// <param name="start">Start of segment.</param>
        /// <param name="end">End of segment.</param>
        /// <param name="center">Center of circle.</param>
        /// <param name="radius">Radius of circle.</param>
        /// <returns>True if they intersect.</returns>
        public static bool SegmentCircle(float2 start, float2 end, float2 center, float radius)
        {
            var d = end - start;
            var f = start - center;

            var a = math.dot(d, d);
            var b = 2 * math.dot(f, d);
            var c = math.dot(f, f) - (radius * radius);

            var discriminant = (b * b) - (4 * a * c);
            return discriminant >= 0;
        }
    }
}