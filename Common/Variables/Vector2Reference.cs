﻿// <copyright file="Vector2Reference.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using UnityEngine;

    /// <summary>
    /// <see cref="Vector2"/> reference.
    /// </summary>
    [CreateAssetMenu(menuName = "BovineLabs/Variables/Vector2 Reference")]
    public class Vector2Reference : ReferenceVariable<Vector2>
    {
    }
}