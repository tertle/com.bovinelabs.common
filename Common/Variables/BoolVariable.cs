﻿// <copyright file="BoolVariable.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using System;

    /// <summary>
    /// <see cref="bool"/> variable.
    /// </summary>
    [Serializable]
    public class BoolVariable : Variable<BoolReference, bool>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BoolVariable"/> class.
        /// </summary>
        /// <param name="value">The default value.</param>
        public BoolVariable(bool value = default)
            : base(value)
        {
        }
    }
}