﻿// <copyright file="BoolReference.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using UnityEngine;

    /// <summary>
    /// <see cref="bool"/> reference.
    /// </summary>
    [CreateAssetMenu(menuName = "BovineLabs/Variables/Bool Reference")]
    public class BoolReference : ReferenceVariable<bool>
    {
    }
}