﻿// <copyright file="Vector3Variable.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using System;
    using UnityEngine;

    /// <summary>
    /// <see cref="Vector3"/> variable.
    /// </summary>
    [Serializable]
    public class Vector3Variable : Variable<Vector3Reference, Vector3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3Variable"/> class.
        /// </summary>
        /// <param name="value">The default value.</param>
        public Vector3Variable(Vector3 value = default)
            : base(value)
        {
        }
    }
}