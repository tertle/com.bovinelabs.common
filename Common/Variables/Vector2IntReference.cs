// <copyright file="Vector2IntReference.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using UnityEngine;

    /// <summary>
    /// <see cref="Vector2Int"/> reference.
    /// </summary>
    [CreateAssetMenu(menuName = "BovineLabs/Variables/Vector2Int Reference")]
    public class Vector2IntReference : ReferenceVariable<Vector2Int>
    {
    }
}