// <copyright file="Vector2IntVariable.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using System;
    using UnityEngine;

    /// <summary>
    /// <see cref="Vector2Int"/> variable.
    /// </summary>
    [Serializable]
    public class Vector2IntVariable : Variable<Vector2IntReference, Vector2Int>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2IntVariable"/> class.
        /// </summary>
        /// <param name="value">The default value.</param>
        public Vector2IntVariable(Vector2Int value = default)
            : base(value)
        {
        }
    }
}