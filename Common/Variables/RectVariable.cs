﻿// <copyright file="RectVariable.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using System;
    using UnityEngine;

    /// <summary>
    /// <see cref="Rect"/> variable.
    /// </summary>
    [Serializable]
    public class RectVariable : Variable<RectReference, Rect>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RectVariable"/> class.
        /// </summary>
        /// <param name="value">The default value.</param>
        public RectVariable(Rect value = default)
            : base(value)
        {
        }
    }
}