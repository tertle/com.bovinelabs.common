﻿// <copyright file="Vector3Reference.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using UnityEngine;

    /// <summary>
    /// <see cref="Vector3"/> reference.
    /// </summary>
    [CreateAssetMenu(menuName = "BovineLabs/Variables/Vector3 Reference")]
    public class Vector3Reference : ReferenceVariable<Vector3>
    {
    }
}