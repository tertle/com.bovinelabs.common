﻿// <copyright file="LayerMaskVariable.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using System;
    using UnityEngine;

    /// <summary>
    /// <see cref="LayerMask"/> variable.
    /// </summary>
    [Serializable]
    public class LayerMaskVariable : Variable<LayerMaskReference, LayerMask>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LayerMaskVariable"/> class.
        /// </summary>
        /// <param name="value">The default value.</param>
        public LayerMaskVariable(LayerMask value = default)
            : base(value)
        {
        }
    }
}
