// <copyright file="Vector3IntReference.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using UnityEngine;

    /// <summary>
    /// <see cref="Vector3Int"/> reference.
    /// </summary>
    [CreateAssetMenu(menuName = "BovineLabs/Variables/Vector3Int Reference")]
    public class Vector3IntReference : ReferenceVariable<Vector3Int>
    {
    }
}