// <copyright file="Vector3IntVariable.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using System;
    using UnityEngine;

    /// <summary>
    /// <see cref="Vector3Int"/> variable.
    /// </summary>
    [Serializable]
    public class Vector3IntVariable : Variable<Vector3IntReference, Vector3Int>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3IntVariable"/> class.
        /// </summary>
        /// <param name="value">The default value.</param>
        public Vector3IntVariable(Vector3Int value = default)
            : base(value)
        {
        }
    }
}