﻿// <copyright file="ColorVariable.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using System;
    using UnityEngine;

    /// <summary>
    /// <see cref="Color"/> variable.
    /// </summary>
    [Serializable]
    public class ColorVariable : Variable<ColorReference, Color>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ColorVariable"/> class.
        /// </summary>
        /// <param name="value">The default value.</param>
        public ColorVariable(Color value = default)
            : base(value)
        {
        }
    }
}