﻿// <copyright file="RectReference.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using UnityEngine;

    /// <summary>
    /// <see cref="Rect"/> reference.
    /// </summary>
    [CreateAssetMenu(menuName = "BovineLabs/Variables/Rect Reference")]
    public class RectReference : ReferenceVariable<Rect>
    {
    }
}