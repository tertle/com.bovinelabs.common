﻿// <copyright file="StringVariable.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using System;

    /// <summary>
    /// <see cref="string"/> variable.
    /// </summary>
    [Serializable]
    public class StringVariable : Variable<StringReference, string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StringVariable"/> class.
        /// </summary>
        /// <param name="value">The default value.</param>
        public StringVariable(string value = default)
            : base(value)
        {
        }
    }
}