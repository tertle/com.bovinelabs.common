﻿// <copyright file="ColorReference.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using UnityEngine;

    /// <summary>
    /// <see cref="Color"/> reference.1
    /// </summary>
    [CreateAssetMenu(menuName = "BovineLabs/Variables/Color Reference")]
    public class ColorReference : ReferenceVariable<Color>
    {
    }
}