﻿// <copyright file="Vector4Reference.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using UnityEngine;

    /// <summary>
    /// <see cref="Vector4"/> reference.
    /// </summary>
    [CreateAssetMenu(menuName = "BovineLabs/Variables/Vector4 Reference")]
    public class Vector4Reference : ReferenceVariable<Vector4>
    {
    }
}