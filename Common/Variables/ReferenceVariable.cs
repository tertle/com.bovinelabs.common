﻿// <copyright file="ReferenceVariable.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using UnityEngine;

    /// <summary>
    /// A <see cref="ScriptableObject"/> wrapper for a struct to allow reference and sharing.
    /// </summary>
    /// <typeparam name="T">The type of reference variable.</typeparam>
    public abstract class ReferenceVariable<T> : ScriptableObject
    {
        [SerializeField]
        private T value;

        /// <summary>
        /// Gets or sets the value of the variable.
        /// </summary>
        public T Value
        {
            get => this.value;
            set => this.value = value;
        }

        public static implicit operator T(ReferenceVariable<T> variable)
        {
            return variable.value;
        }
    }
}