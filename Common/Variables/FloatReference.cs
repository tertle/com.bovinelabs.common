﻿// <copyright file="FloatReference.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using UnityEngine;

    /// <summary>
    /// <see cref="float"/> reference.
    /// </summary>
    [CreateAssetMenu(menuName = "BovineLabs/Variables/Float Reference")]
    public class FloatReference : ReferenceVariable<float>
    {
    }
}