﻿// <copyright file="Variable.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using System;
    using UnityEngine;

    /// <summary>
    /// An inspector friendly variable class that allows variables to be shared between objects or to use constant values.
    /// </summary>
    /// <typeparam name="TR">Type of <see cref="ReferenceVariable{T}"/>.</typeparam>
    /// <typeparam name="T">The type the reference represents.</typeparam>
    [Serializable]
    public abstract class Variable<TR, T>
        where TR : ReferenceVariable<T>
    {
        [SerializeField]
        private bool useConstant = true;

        [SerializeField]
        private T constant;

        [SerializeField]
        private TR variable = default;

        /// <summary>
        /// Initializes a new instance of the <see cref="Variable{TR, T}"/> class.
        /// </summary>
        /// <param name="value">The default value.</param>
        protected Variable(T value = default)
        {
            this.useConstant = true;
            this.constant = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Variable{TR, T}"/> class.
        /// </summary>
        /// <param name="variable">The default value.</param>
        protected Variable(TR variable)
        {
            this.useConstant = false;
            this.variable = variable;
        }

        private Variable()
        {
        }

        /// <summary>
        /// Gets or sets the value of the variable. Setting this value will change the variable to be constant.
        /// </summary>
        public T Value
        {
            get => this.useConstant || this.variable == null ? this.constant : this.variable.Value;
            set
            {
                this.useConstant = true;
                this.constant = value;
            }
        }

        public static implicit operator T(Variable<TR, T> reference)
        {
            return reference.Value;
        }

#if UNITY_EDITOR
        /// <summary>
        /// Sets the value of the variable. Can only be called within the editor.
        /// </summary>
        /// <param name="v">The value of the variable.</param>
        public void SetVariable(TR v)
        {
            this.variable = v;
        }
#endif
    }
}