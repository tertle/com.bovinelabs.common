﻿// <copyright file="LayerMaskReference.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using UnityEngine;

    /// <summary>
    /// <see cref="LayerMask"/> reference.
    /// </summary>
    [CreateAssetMenu(menuName = "BovineLabs/Variables/LayerMask Reference")]
    public class LayerMaskReference : ReferenceVariable<LayerMask>
    {
    }
}
