﻿// <copyright file="FloatVariable.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using System;

    /// <summary>
    /// <see cref="float"/> variable.
    /// </summary>
    [Serializable]
    public class FloatVariable : Variable<FloatReference, float>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FloatVariable"/> class.
        /// </summary>
        public FloatVariable()
            : this(0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FloatVariable"/> class.
        /// </summary>
        /// <param name="value">The default value.</param>
        public FloatVariable(float value)
            : base(value)
        {
        }
    }
}