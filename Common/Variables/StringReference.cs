﻿// <copyright file="StringReference.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using UnityEngine;

    /// <summary>
    /// <see cref="string"/> reference.
    /// </summary>
    [CreateAssetMenu(menuName = "BovineLabs/Variables/String Reference")]
    public class StringReference : ReferenceVariable<string>
    {
    }
}