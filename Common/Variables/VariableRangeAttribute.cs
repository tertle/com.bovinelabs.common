// <copyright file="VariableRangeAttribute.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using System;
    using UnityEngine;

    /// <summary>
    ///   <para>Attribute used to make a float or int variable in a script be restricted to a specific range.</para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class VariableRangeAttribute : PropertyAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VariableRangeAttribute"/> class.
        /// </summary>
        /// <param name="min">The min value.</param>
        /// <param name="max">The max value.</param>
        public VariableRangeAttribute(float min, float max)
        {
            this.Min = min;
            this.Max = max;
        }

        /// <summary>
        /// Gets the min value.
        /// </summary>
        public float Min { get; }

        /// <summary>
        /// Gets the max value.
        /// </summary>
        public float Max { get; }
    }
}