﻿// <copyright file="IntReference.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using UnityEngine;

    /// <summary>
    /// <see cref="int"/> reference.
    /// </summary>
    [CreateAssetMenu(menuName = "BovineLabs/Variables/Int Reference")]
    public class IntReference : ReferenceVariable<int>
    {
    }
}
