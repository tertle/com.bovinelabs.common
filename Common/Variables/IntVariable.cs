﻿// <copyright file="IntVariable.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using System;

    /// <summary>
    /// <see cref="int"/> variable.
    /// </summary>
    [Serializable]
    public class IntVariable : Variable<IntReference, int>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IntVariable"/> class.
        /// </summary>
        public IntVariable()
            : this(0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IntVariable"/> class.
        /// </summary>
        /// <param name="value">The default value.</param>
        public IntVariable(int value)
            : base(value)
        {
        }
    }
}