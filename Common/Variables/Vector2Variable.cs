﻿// <copyright file="Vector2Variable.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using System;
    using UnityEngine;

    /// <summary>
    /// <see cref="Vector2"/> variable.
    /// </summary>
    [Serializable]
    public class Vector2Variable : Variable<Vector2Reference, Vector2>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2Variable"/> class.
        /// </summary>
        /// <param name="value">The default value.</param>
        public Vector2Variable(Vector2 value = default)
            : base(value)
        {
        }
    }
}