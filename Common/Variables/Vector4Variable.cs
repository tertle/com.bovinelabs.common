﻿// <copyright file="Vector4Variable.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Variables
{
    using System;
    using UnityEngine;

    /// <summary>
    /// <see cref="Vector4"/> variable.
    /// </summary>
    [Serializable]
    public class Vector4Variable : Variable<Vector4Reference, Vector4>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4Variable"/> class.
        /// </summary>
        /// <param name="value">The default value.</param>
        public Vector4Variable(Vector4 value = default)
            : base(value)
        {
        }
    }
}