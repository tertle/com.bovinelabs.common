// <copyright file="RemoveComponentChunkJob.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Jobs
{
    using Unity.Collections;
    using Unity.Entities;

    /// <summary>
    /// Remove a component from entities in a query.
    /// </summary>
    /// <typeparam name="T">The type of component to remove.</typeparam>
/*#if UNITY_2019_3_OR_NEWER
    [Unity.Burst.BurstCompile]
#endif*/
    public struct RemoveComponentChunkJob<T> : IJobChunk
    {
        [ReadOnly]
        public ArchetypeChunkEntityType EntityType;

        /// <summary>
        /// The entity command buffer.
        /// </summary>
        public EntityCommandBuffer.Concurrent EntityCommandBuffer;

        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var entities = chunk.GetNativeArray(this.EntityType);
            for (var i = 0; i < entities.Length; i++)
            {
                this.EntityCommandBuffer.RemoveComponent<T>(chunkIndex, entities[i]);
            }
        }
    }
}