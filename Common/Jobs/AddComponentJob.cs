// <copyright file="AddComponentJob.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Jobs
{
    using Unity.Collections;
    using Unity.Entities;
    using Unity.Jobs;

    /// <summary>
    /// Iterate a queue of entities and add a default <see cref="T"/> component to them.
    /// </summary>
    /// <typeparam name="T">The component type to add.</typeparam>
/*#if UNITY_2019_3_OR_NEWER
    [Unity.Burst.BurstCompile]
#endif*/
    public struct AddComponentJob<T> : IJob
        where T : struct, IComponentData
    {
        /// <summary>
        /// The queue of entities to add the <see cref="T"/> component to.
        /// </summary>
        public NativeQueue<Entity> Entities;

        /// <summary>
        /// The entity command buffer.
        /// </summary>
        public EntityCommandBuffer EntityCommandBuffer;

        /// <inheritdoc/>
        public void Execute()
        {
            while (this.Entities.TryDequeue(out var entity))
            {
                this.EntityCommandBuffer.AddComponent(entity, default(T));
            }
        }
    }
}