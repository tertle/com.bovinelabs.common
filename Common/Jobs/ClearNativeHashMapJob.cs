// <copyright file="ClearNativeHashMapJob.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Jobs
{
    using System;
    using Unity.Burst;
    using Unity.Collections;
    using Unity.Jobs;

    /// <summary>
    /// Clear a <see cref="NativeHashMap{TKey,TValue}"/>.
    /// </summary>
    /// <typeparam name="TKey">The key type.</typeparam>
    /// <typeparam name="TValue">The value type.</typeparam>
    [BurstCompile]
    public struct ClearNativeHashMapJob<TKey, TValue> : IJob
        where TKey : struct, IEquatable<TKey>
        where TValue : struct
    {
        /// <summary>
        /// The map to clear.
        /// </summary>
        public NativeHashMap<TKey, TValue> Map;

        /// <inheritdoc />
        public void Execute()
        {
            this.Map.Clear();
        }
    }
}