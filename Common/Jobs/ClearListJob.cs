// <copyright file="ClearListJob.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Jobs
{
    using Unity.Burst;
    using Unity.Collections;
    using Unity.Jobs;

    /// <summary>
    /// Clear a <see cref="NativeList{T}"/>.
    /// </summary>
    /// <typeparam name="T">The value type.</typeparam>
    [BurstCompile]
    public struct ClearListJob<T> : IJob
        where T : struct
    {
        /// <summary>
        /// The map to clear.
        /// </summary>
        public NativeList<T> List;

        /// <inheritdoc />
        public void Execute()
        {
            this.List.Clear();
        }
    }
}