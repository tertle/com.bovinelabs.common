// <copyright file="CopyQueueToHashMapJob.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Jobs
{
    using System;
    using BovineLabs.Common.Helpers;
    using Unity.Burst;
    using Unity.Collections;
    using Unity.Jobs;

    /// <summary>
    /// Copy a <see cref="NativeQueue{T}"/> to a <see cref="NativeList{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of the containers.</typeparam>
    [BurstCompile]
    public struct CopyQueueToHashMapJob<T> : IJob
        where T : struct, IEquatable<T>
    {
        /// <summary>
        /// The input queue.
        /// </summary>
        public NativeQueue<T> Queue;

        /// <summary>
        /// The output list.
        /// </summary>
        public NativeHashMap<T, Empty> Map;

        /// <inheritdoc/>
        public void Execute()
        {
            this.Map.Clear();
            if (this.Map.Capacity < this.Queue.Count)
            {
                this.Map.Capacity = this.Queue.Count;
            }

            while (this.Queue.TryDequeue(out var v))
            {
                this.Map.TryAdd(v, default);
            }
        }
    }
}