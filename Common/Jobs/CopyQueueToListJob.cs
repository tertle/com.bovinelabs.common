// <copyright file="CopyQueueToListJob.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Jobs
{
    using Unity.Burst;
    using Unity.Collections;
    using Unity.Jobs;

    /// <summary>
    /// Copy a <see cref="NativeQueue{T}"/> to a <see cref="NativeList{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of the containers.</typeparam>
    [BurstCompile]
    public struct CopyQueueToListJob<T> : IJob
        where T : struct
    {
        /// <summary>
        /// The input queue.
        /// </summary>
        public NativeQueue<T> Queue;

        /// <summary>
        /// The output list.
        /// </summary>
        public NativeList<T> List;

        /// <inheritdoc/>
        public void Execute()
        {
            this.List.Clear();
            if (this.List.Capacity < this.Queue.Count)
            {
                this.List.Capacity = this.Queue.Count;
            }

            while (this.Queue.TryDequeue(out var v))
            {
                this.List.Add(v);
            }
        }
    }
}