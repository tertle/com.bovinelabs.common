﻿// <copyright file="RemoveComponentJob.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Jobs
{
    using Unity.Collections;
    using Unity.Entities;
    using Unity.Jobs;

    /// <summary>
    /// Iterate a queue of entities and remove the <see cref="T"/> component from them.
    /// </summary>
    /// <typeparam name="T">The component type to remove.</typeparam>
/*#if UNITY_2019_3_OR_NEWER
    [Unity.Burst.BurstCompile]
#endif*/
    public struct RemoveComponentJob<T> : IJob
        where T : struct, IComponentData
    {
        /// <summary>
        /// The queue of entities to remove the <see cref="T"/> component from.
        /// </summary>
        public NativeQueue<Entity> Entities;

        /// <summary>
        /// The <see cref="EntityCommandBuffer"/>.
        /// </summary>
        public EntityCommandBuffer EntityCommandBuffer;

        /// <inheritdoc />
        public void Execute()
        {
            while (this.Entities.TryDequeue(out var entity))
            {
                this.EntityCommandBuffer.RemoveComponent<T>(entity);
            }
        }
    }
}