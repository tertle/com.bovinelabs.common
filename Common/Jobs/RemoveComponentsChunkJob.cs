// <copyright file="RemoveComponentsChunkJob.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Jobs
{
    using Unity.Collections;
    using Unity.Entities;

    /// <summary>
    /// Remove two components from entities in a query.
    /// </summary>
    /// <typeparam name="T0">The first type of component to remove.</typeparam>
    /// <typeparam name="T1">The second type of component to remove.</typeparam>
/*#if UNITY_2019_3_OR_NEWER
    [Unity.Burst.BurstCompile]
#endif*/
    public struct RemoveComponentsChunkJob<T0, T1> : IJobChunk
    {
        [ReadOnly]
        public ArchetypeChunkEntityType EntityType;

        /// <summary>
        /// The entity command buffer.
        /// </summary>
        public EntityCommandBuffer.Concurrent EntityCommandBuffer;

        /// <inheritdoc />
        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var entities = chunk.GetNativeArray(this.EntityType);
            for (var i = 0; i < entities.Length; i++)
            {
                this.EntityCommandBuffer.RemoveComponent<T0>(chunkIndex, entities[i]);
                this.EntityCommandBuffer.RemoveComponent<T1>(chunkIndex, entities[i]);
            }
        }
    }
}