﻿// <copyright file="NativeArrayExtensions.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Extensions
{
    using Unity.Collections;
    using Unity.Collections.LowLevel.Unsafe;

    /// <summary>
    /// The NativeArrayExtensions.
    /// </summary>
    public static class NativeArrayExtensions
    {
        /// <summary>
        /// Clear an native array to default value using MemClear.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <typeparam name="T">The array type.</typeparam>
        public static unsafe void Clear<T>(this NativeArray<T> array)
            where T : struct
        {
            UnsafeUtility.MemClear(array.GetUnsafePtr(), (long)array.Length * UnsafeUtility.SizeOf<T>());
        }

        /// <summary>
        /// Sets all values of a NativeArray{int} to 1
        /// For an integer array this is the same as setting the entire thing to -1.
        /// </summary>
        /// <param name="array">The NativeArray to set.</param>
        public static void SetNegativeOne(this NativeArray<int> array)
        {
            var length = array.Length;

#if UNITY_2019_3_OR_NEWER
            unsafe
            {
                UnsafeUtility.MemSet(array.GetUnsafePtr(), byte.MaxValue, length * UnsafeUtility.SizeOf<int>());
            }
#else
            for (var i = 0; i < length; i++)
            {
                array[i] = -1;
            }
#endif
        }
    }
}