// <copyright file="LocalToWorldExtensions.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Extensions
{
    using Unity.Mathematics;
    using Unity.Transforms;

    /// <summary>
    /// Extensions for <see cref="LocalToWorld"/>.
    /// </summary>
    public static class LocalToWorldExtensions
    {
        /// <summary>
        /// Gets the rotation.
        /// </summary>
        /// <param name="localToWorld">The local to world component.</param>
        /// <returns>The rotation.</returns>
        public static quaternion Rotation(this LocalToWorld localToWorld)
        {
            /*float3 scale = localToWorld.Value.GetScale();
            float3 inverted = scale.Invert();
            float4x4 value = localToWorld.Value.ScaleBy(inverted); // remove the scale

            // TODO use?
            // quaternion.LookRotation()
            return new quaternion(value);*/

            var forward = math.normalize(localToWorld.Forward);
            var up = math.normalize(localToWorld.Up);
            return quaternion.LookRotation(forward, up);
        }
    }
}