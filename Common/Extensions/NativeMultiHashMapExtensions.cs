// <copyright file="NativeMultiHashMapExtensions.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Extensions
{
    using System;
    using BovineLabs.Common.Collections;
    using BovineLabs.Common.Jobs;
    using Unity.Collections;
    using Unity.Jobs;

    /// <summary>
    /// Extensions for NativeMultiHashMap.
    /// </summary>
    public static class NativeMultiHashMapExtensions
    {
        /// <summary>
        /// Create a clear job for <see cref="NativeMultiHashMap{TKey,TValue}"/>.
        /// </summary>
        /// <param name="hashMap">The hash map.</param>
        /// <param name="handle">The dependency handle.</param>
        /// <typeparam name="TKey">The key type.</typeparam>
        /// <typeparam name="TValue">The value type.</typeparam>
        /// <returns>The handle to the job.</returns>
        public static JobHandle ClearInJob<TKey, TValue>(
            this NativeMultiHashMap<TKey, TValue> hashMap,
            JobHandle handle)
            where TKey : struct, IEquatable<TKey>
            where TValue : struct
        {
            return new ClearNativeMultiHashMapJob<TKey, TValue>
                {
                    Map = hashMap,
                }
                .Schedule(handle);
        }

        /// <summary>
        /// Get an iterator for a <see cref="NativeMultiHashMap{TKey,TValue}"/>.
        /// </summary>
        /// <param name="hashMap">The hash map.</param>
        /// <typeparam name="TKey">The key type.</typeparam>
        /// <typeparam name="TValue">The value type.</typeparam>
        /// <returns>The iterator.</returns>
        public static unsafe NativeHashMapIterator<TKey, TValue> GetIterator<TKey, TValue>(
            this NativeMultiHashMap<TKey, TValue> hashMap)
            where TKey : struct, IEquatable<TKey>
            where TValue : struct
        {
            var imposter = (NativeMultiHashMapImposter<TKey, TValue>)hashMap;

            NativeHashMapDataImposter* data = imposter.Buffer;

            return new NativeHashMapIterator<TKey, TValue>(data);
        }

        /// <summary>
        /// Fast clear a <see cref="NativeMultiHashMap{TKey,TValue}"/>.
        /// </summary>
        /// <remarks>Note this is only improved in 2019.3 or later.</remarks>
        /// <param name="hashMap">The hash map.</param>
        /// <typeparam name="TKey">The key type.</typeparam>
        /// <typeparam name="TValue">The value type.</typeparam>
        public static void FastClear<TKey, TValue>(this NativeMultiHashMap<TKey, TValue> hashMap)
            where TKey : struct, IEquatable<TKey>
            where TValue : struct
        {
#if UNITY_2019_3_OR_NEWER
            var imposter = (NativeMultiHashMapImposter<TKey, TValue>)hashMap;

            unsafe
            {
                NativeHashMapDataImposter.Clear(imposter.Buffer);
            }
#else
            hashMap.Clear();
#endif
        }

        /// <summary>
        /// Clone a <see cref="NativeMultiHashMap{TKey,TValue}"/> to a new instance.
        /// </summary>
        /// <param name="hashMap">The hash map to clone.</param>
        /// <param name="allocator">The allocator for the clone.</param>
        /// <typeparam name="TKey">Type of key.</typeparam>
        /// <typeparam name="TValue">Type of value.</typeparam>
        /// <returns>A clone of the original hash map.</returns>
        public static unsafe NativeMultiHashMap<TKey, TValue> Clone<TKey, TValue>(
            this NativeMultiHashMap<TKey, TValue> hashMap, Allocator allocator)
            where TKey : struct, IEquatable<TKey>
            where TValue : struct
        {
            var clone = new NativeMultiHashMap<TKey, TValue>(hashMap.Capacity, allocator);
            var imposter = (NativeMultiHashMapImposter<TKey, TValue>)hashMap;
            var cloneImposter = (NativeMultiHashMapImposter<TKey, TValue>)clone;

            NativeHashMapDataImposter.Clone<TKey, TValue>(imposter.Buffer, cloneImposter.Buffer);

            return clone;
        }
    }
}