﻿// <copyright file="MathematicsExtensions.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Extensions
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Unity.Mathematics;
    using UnityEngine.Assertions;

    /// <summary>
    /// Extensions for types within the <see cref="Unity.Mathematics"/> namespace.
    /// </summary>
    public static class MathematicsExtensions
    {
        /// <summary>
        /// Floor a <see cref="float"/> to an integer.
        /// </summary>
        /// <param name="v">The <see cref="float"/> to floor.</param>
        /// <returns>A <see cref="int"/>.</returns>
        public static int FloorToInt(this float v) => (int)math.floor(v);

        /// <summary>
        /// Floor a <see cref="float"/> to an integer.
        /// </summary>
        /// <param name="v">The <see cref="float"/> to floor.</param>
        /// <returns>A <see cref="int"/>.</returns>
        public static int CeilToInt(this float v) => (int)math.ceil(v);

        /// <summary>
        /// Floor a <see cref="float2"/> to integers.
        /// </summary>
        /// <param name="f2">The <see cref="float2"/> to floor.</param>
        /// <returns>A <see cref="int2"/>.</returns>
        public static int2 FloorToInt(this float2 f2) => (int2)math.floor(f2);

        /// <summary>
        /// Floor a <see cref="float2"/> to integers.
        /// </summary>
        /// <param name="f2">The <see cref="float2"/> to floor.</param>
        /// <returns>A <see cref="int2"/>.</returns>
        public static int2 CeilToInt(this float2 f2) => (int2)math.ceil(f2);

        /// <summary>
        /// Floor a <see cref="float3"/> to integers.
        /// </summary>
        /// <param name="f3">The <see cref="float3"/> to floor.</param>
        /// <returns>A <see cref="int3"/>.</returns>
        public static int3 FloorToInt(this float3 f3) => (int3)math.floor(f3);

        /// <summary>
        /// Floor a <see cref="float"/> to integers.
        /// </summary>
        /// <param name="f">The <see cref="float"/> to floor.</param>
        /// <returns>A <see cref="int"/>.</returns>
        public static int RoundToInt(this float f) => (int)math.round(f);

        /// <summary>
        /// Floor a <see cref="float2"/> to integers.
        /// </summary>
        /// <param name="f2">The <see cref="float2"/> to floor.</param>
        /// <returns>A <see cref="int2"/>.</returns>
        public static int2 RoundToInt(this float2 f2) => (int2)math.round(f2);

        /// <summary>
        /// Floor a <see cref="float3"/> to integers.
        /// </summary>
        /// <param name="f3">The <see cref="float3"/> to floor.</param>
        /// <returns>A <see cref="int3"/>.</returns>
        public static int3 RoundToInt(this float3 f3) => (int3)math.round(f3);

        /// <summary>
        /// Gets the rotation from a transform matrix.
        /// </summary>
        /// <param name="matrix">The transform matrix.</param>
        /// <returns>The rotation.</returns>
        /// <remarks>TODO this only works when scale = 1, need better solution.</remarks>
        public static quaternion GetRotation(this float4x4 matrix)
        {
            return quaternion.LookRotation(new float3(matrix.c2.x, matrix.c2.y, matrix.c2.z), new float3(0, 1, 0));
        }

        /// <summary>
        /// Gets the scale from a transform matrix.
        /// </summary>
        /// <param name="matrix">The transform matrix.</param>
        /// <returns>The lossy scale.</returns>
        public static float3 GetScale(this float4x4 matrix) => new float3(
            math.length(matrix.c0.xyz),
            math.length(matrix.c1.xyz),
            math.length(matrix.c2.xyz));

        /// <summary>
        /// Gets the squared scale from a transform matrix.
        /// </summary>
        /// <param name="matrix">The transform matrix.</param>
        /// <returns>The lossy scale.</returns>
        public static float3 GetScaleSqr(this float4x4 matrix) => new float3(
            math.lengthsq(matrix.c0.xyz),
            math.lengthsq(matrix.c1.xyz),
            math.lengthsq(matrix.c2.xyz));

        /// <summary>
        /// Scales a transform matrix.
        /// </summary>
        /// <param name="matrix">The transform matrix.</param>
        /// <param name="scale">The scale.</param>
        /// <returns>The scaled matrix.</returns>
        public static float4x4 ScaleBy(this float4x4 matrix, float3 scale) => math.mul(matrix, float4x4.Scale(scale));

        /// <summary>
        /// Gets the largest value of a scale from a transform matrix.
        /// </summary>
        /// <param name="matrix">The transform matrix.</param>
        /// <returns>The largest scale.</returns>
        public static float LargestScale(this float4x4 matrix)
        {
            var scaleSqr = matrix.GetScaleSqr();
            float largestScaleSqr = math.cmax(scaleSqr);
            return math.sqrt(largestScaleSqr);
        }

        /// <summary>
        /// Transforms a position by this matrix.
        /// </summary>
        /// <remarks>
        /// Mimics <see cref="UnityEngine.Matrix4x4.MultiplyPoint3x4"/>.
        /// </remarks>
        /// <param name="matrix">The matrix.</param>
        /// <param name="point">The point to transform.</param>
        /// <returns>The transformed point.</returns>
        [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "Using same naming as Matrix4x4.MultiplyPoint3x4")]
        public static float3 MultiplyPoint3x4(this float4x4 matrix, float3 point)
        {
            return math.mul(matrix, new float4(point, 1.0f)).xyz;
        }

        /// <summary>
        /// Safely inverts a vector. 0 returns 0.
        /// </summary>
        /// <param name="f">The vector to invert.</param>
        /// <returns>The inverse of <see cref="f"/>.</returns>
        public static float3 InvertSafe(float3 f)
        {
            var x = Math.Abs(f.x) < float.Epsilon ? 0 : 1 / f.x;
            var y = Math.Abs(f.y) < float.Epsilon ? 0 : 1 / f.y;
            var z = Math.Abs(f.z) < float.Epsilon ? 0 : 1 / f.z;

            return new float3(x, y, z);
        }

        /// <summary>
        /// Inverts a vector.
        /// </summary>
        /// <param name="f">The vector to invert.</param>
        /// <returns>The inverse of <see cref="f"/>.</returns>
        public static float3 Invert(this float3 f)
        {
            Assert.IsFalse(math.any(f == float3.zero));

            return new float3(1 / f.x, 1 / f.y, 1 / f.z);
        }

        /// <summary>
        /// Converts a quaternion to euler.
        /// </summary>
        /// <param name="quaternion">The quaternion.</param>
        /// <returns>Euler angles.</returns>
        public static float3 ToEuler(this quaternion quaternion)
        {
            var q = quaternion.value;

            // roll (x-axis rotation)
            var sinRCosP = 2 * ((q.w * q.x) + (q.y * q.z));
            var cosRCosP = 1 - (2 * ((q.x * q.x) + (q.y * q.y)));
            var roll = math.atan2(sinRCosP, cosRCosP);

            // pitch (y-axis rotation)
            var sinP = 2 * ((q.w * q.y) - (q.z * q.x));
            var pitch = math.select(math.asin(sinP), math.sign(sinP) * math.PI / 2, math.abs(sinP) >= 1);

            // yaw (z-axis rotation)
            var sinYCosP = 2 * ((q.w * q.z) + (q.x * q.y));
            var cosYCosP = 1 - (2 * ((q.y * q.y) + (q.z * q.z)));
            var yaw = math.atan2(sinYCosP, cosYCosP);

            return new float3(roll, pitch, yaw);
        }

        /// <summary>
        /// The forward direction vector.
        /// </summary>
        /// <param name="quaternion">The quaternion.</param>
        /// <returns>Forward vector.</returns>
        public static float3 Forward(this quaternion quaternion)
        {
            return math.mul(quaternion, new float3(0, 0, 1));
        }

        /// <summary>
        /// The right direction vector.
        /// </summary>
        /// <param name="quaternion">The quaternion.</param>
        /// <returns>Right vector.</returns>
        public static float3 Right(this quaternion quaternion)
        {
            return math.mul(quaternion, new float3(1, 0, 0));
        }

        /// <summary>
        /// The up direction vector.
        /// </summary>
        /// <param name="quaternion">The quaternion.</param>
        /// <returns>Up vector.</returns>
        public static float3 Up(this quaternion quaternion)
        {
            return math.mul(quaternion, new float3(0, 1, 0));
        }
    }
}