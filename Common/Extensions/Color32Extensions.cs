// <copyright file="Color32Extensions.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Extensions
{
    using UnityEngine;

    /// <summary>
    /// Color32 Extensions.
    /// </summary>
    public static class Color32Extensions
    {
        /// <summary>
        /// Compares 2 colors.
        /// </summary>
        /// <param name="c1">The first color to compare..</param>
        /// <param name="c2">The second color to compare to.</param>
        /// <returns>True if the colors are identical.</returns>
        public static bool IsEqual(this Color32 c1, Color32 c2)
        {
            return c1.r == c2.r && c1.g == c2.g && c1.b == c2.b && c1.a == c2.a;
        }
    }
}