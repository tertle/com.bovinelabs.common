// <copyright file="ProxyComponent.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Components
{
    using Unity.Entities;
    using UnityEngine;

    /// <summary>
    /// A proxy for <see cref="IComponentData"/>.
    /// </summary>
    /// <typeparam name="T">The type of <see cref="IComponentData"/>.</typeparam>
    [RequireComponent(typeof(ConvertToEntity))]
    public abstract class ProxyComponent<T> : MonoBehaviour, IConvertGameObjectToEntity
        where T : struct, IComponentData
    {
        [SerializeField]
        private T component = default;

        /// <inheritdoc />
        public virtual void Convert(
            Entity entity,
            EntityManager dstManager,
            GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, this.component);
        }
    }
}