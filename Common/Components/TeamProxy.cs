﻿// <copyright file="TeamProxy.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>
// <summary>
//     Collection of components used by the Faction Systems
// </summary>

namespace BovineLabs.Common.Components
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Unity.Entities;
    using UnityEngine;

    /// <summary>
    /// The team an entity belongs to.
    /// </summary>
    [Serializable]
    public struct Team : IComponentData, IEquatable<Team>
    {
        /// <summary>
        /// The max number of teams.
        /// </summary>
        public const int MaxNumberOfTeams = 32;

        /// <summary>
        /// The faction. This should be limited to between 0 and <see cref="MaxNumberOfTeams"/> - 1.
        /// </summary>
        [Range(0, 31)]
        public int Value;

        /// <inheritdoc />
        public bool Equals(Team other)
        {
            return this.Value == other.Value;
        }

        /// <inheritdoc />
        [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode", Justification = "Required by unity inspector.")]
        public override int GetHashCode()
        {
            return this.Value;
        }
    }

    /// <summary>
    /// Proxy for <see cref="Team"/>.
    /// </summary>
    [DisallowMultipleComponent]
    public class TeamProxy : ProxyComponent<Team>
    {
    }
}