// <copyright file="ProxyBuffer.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Components
{
    using Unity.Entities;
    using UnityEngine;

    /// <summary>
    /// A proxy for <see cref="DynamicBuffer{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of <see cref="IBufferElementData"/>.</typeparam>
    [RequireComponent(typeof(ConvertToEntity))]
    public abstract class ProxyBuffer<T> : MonoBehaviour, IConvertGameObjectToEntity
        where T : struct, IBufferElementData
    {
        /// <inheritdoc />
        public virtual void Convert(
            Entity entity,
            EntityManager dstManager,
            GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddBuffer<T>(entity);
        }
    }
}