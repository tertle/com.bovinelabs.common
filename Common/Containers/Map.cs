// <copyright file="Map.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Containers
{
    using System.Collections.Generic;

    /// <summary>
    /// The map represents a bi-directional dictionary.
    /// </summary>
    /// <typeparam name="T1">The forward type.</typeparam>
    /// <typeparam name="T2">The reverse type.</typeparam>
    /// <remarks>
    /// Base code from. <a href="https://stackoverflow.com/questions/10966331/two-way-bidirectional-dictionary-in-c"/>
    /// </remarks>
    public class Map<T1, T2>
    {
        private readonly Dictionary<T1, T2> forward;
        private readonly Dictionary<T2, T1> reverse;

        /// <summary>
        /// Initializes a new instance of the <see cref="Map{T1, T2}"/> class.
        /// </summary>
        public Map()
            : this(null, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Map{T1, T2}"/> class.
        /// </summary>
        /// <param name="forwardComparer">A comparer for the forward dictionary.</param>
        public Map(IEqualityComparer<T1> forwardComparer)
            : this(forwardComparer, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Map{T1, T2}"/> class.
        /// </summary>
        /// <param name="reverseComparer">A comparer for the reverse dictionary.</param>
        public Map(IEqualityComparer<T2> reverseComparer)
            : this(null, reverseComparer)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Map{T1, T2}"/> class.
        /// </summary>
        /// <param name="forwardComparer">A comparer for the forward dictionary.</param>
        /// <param name="reverseComparer">A comparer for the reverse dictionary.</param>
        public Map(IEqualityComparer<T1> forwardComparer, IEqualityComparer<T2> reverseComparer)
        {
            this.forward = new Dictionary<T1, T2>(forwardComparer);
            this.reverse = new Dictionary<T2, T1>(reverseComparer);

            this.Forward = new Indexer<T1, T2>(this.forward);
            this.Reverse = new Indexer<T2, T1>(this.reverse);
        }

        /// <summary>
        /// Gets a forward indexer of the map.
        /// </summary>
        public Indexer<T1, T2> Forward { get; }

        /// <summary>
        /// Gets a reverse indexer of the map.
        /// </summary>
        public Indexer<T2, T1> Reverse { get; }

        /// <summary>
        /// Get or set the value of a map using the forward indexer.
        /// </summary>
        /// <param name="t">The forward value used as a key.</param>
        /// <returns>The reverse value.</returns>
        public T2 this[T1 t]
        {
            get => this.forward[t];
            set
            {
                this.reverse[value] = t;
                this.forward[t] = value;
            }
        }

        /// <summary>
        /// Get or set the value of a map using the reverse indexer.
        /// </summary>
        /// <param name="t">The reverse value used as a key.</param>
        /// <returns>The forward value.</returns>
        public T1 this[T2 t]
        {
            get => this.reverse[t];
            set
            {
                this.forward[value] = t;
                this.reverse[t] = value;
            }
        }

        /// <summary>
        /// Add an element to the map.
        /// </summary>
        /// <param name="t1">The forward value.</param>
        /// <param name="t2">The reverse value.</param>
        public void Add(T1 t1, T2 t2)
        {
            this.forward.Add(t1, t2);
            this.reverse.Add(t2, t1);
        }

        /// <summary>
        /// Remove an element from the dictionary using its' forward value.
        /// </summary>
        /// <param name="t1">The forward value to remove.</param>
        public void RemoveForward(T1 t1)
        {
            if (!this.forward.TryGetValue(t1, out var t2))
            {
                return;
            }

            this.forward.Remove(t1);
            this.reverse.Remove(t2);
        }

        /// <summary>
        /// Remove an element from the dictionary using its' reverse value.
        /// </summary>
        /// <param name="t2">The reverse value to remove.</param>
        public void RemoveReverse(T2 t2)
        {
            if (!this.reverse.TryGetValue(t2, out var t1))
            {
                return;
            }

            this.forward.Remove(t1);
            this.reverse.Remove(t2);
        }

        /// <summary>
        /// Try get a value forward.
        /// </summary>
        /// <param name="t1">The forward key.</param>
        /// <param name="t2">The returned reverse value.</param>
        /// <returns>True if value found for key.</returns>
        public bool TryGetValue(T1 t1, out T2 t2)
        {
            return this.forward.TryGetValue(t1, out t2);
        }

        /// <summary>
        /// Try get a value reverse.
        /// </summary>
        /// <param name="t2">The reverse key.</param>
        /// <param name="t1">The returned forward value.</param>
        /// <returns>True if value found for key.</returns>
        public bool TryGetValue(T2 t2, out T1 t1)
        {
            return this.reverse.TryGetValue(t2, out t1);
        }

        /// <summary>
        /// ClearInJob all the elements from the map.
        /// </summary>
        public void Clear()
        {
            this.forward.Clear();
            this.reverse.Clear();
        }

        /// <summary>
        /// Indexer for tracking elements.
        /// </summary>
        /// <typeparam name="T3">Type of the key.</typeparam>
        /// <typeparam name="T4">Type of the value.</typeparam>
        public class Indexer<T3, T4>
        {
            private readonly Dictionary<T3, T4> dictionary;

            /// <summary>
            /// Initializes a new instance of the <see cref="Indexer{T3, T4}"/> class.
            /// </summary>
            /// <param name="dictionary">The dictionary the indexer references.</param>
            public Indexer(Dictionary<T3, T4> dictionary)
            {
                this.dictionary = dictionary;
            }

            /// <summary>
            /// Indexer to get the value from the dictionary.
            /// </summary>
            /// <param name="index">Index to get the value.</param>
            /// <returns>The returned value.</returns>
            public T4 this[T3 index] => this.dictionary[index];

            /// <summary>
            /// Try get a value at an index.
            /// </summary>
            /// <param name="index">Index to get the value.</param>
            /// <param name="value">The returned value.</param>
            /// <returns>True if element found, else false.</returns>
            public bool TryGetValue(T3 index, out T4 value)
            {
                return this.dictionary.TryGetValue(index, out value);
            }
        }
    }
}