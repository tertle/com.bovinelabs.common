// <copyright file="NativeHashMapIterator.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>


namespace BovineLabs.Common.Collections
{
    using Unity.Collections;
    using Unity.Collections.LowLevel.Unsafe;

    /// <summary>
    /// An iterator for both <see cref="NativeHashMap{TKey,TValue}"/> and <see cref="NativeMultiHashMap{TKey, TValue}"/>.
    /// Used like this
    /// while (iterator.MoveNext())
    /// {
    ///     var key = iterator.Key;
    ///     var val = iterator.Value;
    /// }
    /// </summary>
    /// <typeparam name="TKey">The type of the hash map key.</typeparam>
    /// <typeparam name="TValue">The type of the hash map value.</typeparam>
    public unsafe struct NativeHashMapIterator<TKey, TValue>
        where TKey : struct
        where TValue : struct
    {
        private readonly NativeHashMapDataImposter* data;
        private readonly int* bucketArray;
        private readonly int* bucketNext;

        private int i;

        private int entryIndex;
        //private int b;

        /// <summary>
        /// Initializes a new instance of the <see cref="NativeHashMapIterator{TKey, TValue}"/> struct.
        /// </summary>
        /// <param name="data">The HashMap data.</param>
        internal NativeHashMapIterator(NativeHashMapDataImposter* data)
        {
            this.data = data;

            this.bucketArray = (int*)data->Buckets;
            this.bucketNext = (int*)data->Next;

            this.i = 0;
            this.entryIndex = -1;
        }

        /// <summary>
        /// Gets the key at the current position. Make sure MoveNext returned true before calling this.
        /// </summary>
        public TKey Key => UnsafeUtility.ReadArrayElement<TKey>(this.data->Keys, this.entryIndex);

        /// <summary>
        /// Gets the value at the current position. Make sure MoveNext returned true before calling this.
        /// </summary>
        public TValue Value => UnsafeUtility.ReadArrayElement<TValue>(this.data->Values, this.entryIndex);

        /// <summary>
        /// Moves to the next element.
        /// </summary>
        /// <returns>Returns true if element exists, otherwise false.</returns>
        public bool MoveNext()
        {
            for (; this.i < this.data->BucketCapacityMask + 1; this.i++)
            {
                this.entryIndex = this.entryIndex == -1 ?
                    this.bucketArray[this.i] :
                    this.bucketNext[this.entryIndex];

                if (this.entryIndex != -1)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Resets the iterator to start again.
        /// </summary>
        public void Reset()
        {
            this.i = 0;
            this.entryIndex = -1;
        }
    }
}