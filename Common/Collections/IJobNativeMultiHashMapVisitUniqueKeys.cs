// <copyright file="IJobNativeMultiHashMapVisitUniqueKeys.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Collections
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Unity.Collections;
    using Unity.Collections.LowLevel.Unsafe;
    using Unity.Jobs;
    using Unity.Jobs.LowLevel.Unsafe;

    /// <summary>
    /// Job that visits each unique key in a <see cref="NativeMultiHashMap{TKey,TValue}"/>.
    /// </summary>
    /// <typeparam name="TKey">The key type of the hash map.</typeparam>
    /// <typeparam name="TValue">The value type of the hash map.</typeparam>
    [JobProducerType(typeof(JobNativeMultiHashMapVisitUniqueKey.NativeMultiHashMapVisitUniqueKeyJobStruct<,,>))]
    [SuppressMessage("ReSharper", "TypeParameterCanBeVariant", Justification = "Unity job system doesn't like it.")]
    [SuppressMessage("ReSharper", "UnusedTypeParameter", Justification = "Required by burst.")]
    public interface IJobNativeMultiHashMapVisitUniqueKeys<TKey, TValue>
        where TKey : struct, IEquatable<TKey>
        where TValue : struct
    {
        /// <summary>
        /// Execute the job on a unique key from the hash map.
        /// </summary>
        /// <param name="key">The key from the hash map.</param>
        void Execute(TKey key);
    }

    /// <summary>
    /// Extension methods for <see cref="IJobNativeMultiHashMapVisitUniqueKeys{TKey,TValue}"/>.
    /// </summary>
    public static class JobNativeMultiHashMapVisitUniqueKey
    {
        /// <summary>
        /// Schedule a <see cref="IJobNativeMultiHashMapVisitUniqueKeys{TKey,TValue}"/> job.
        /// </summary>
        /// <param name="jobData">The job.</param>
        /// <param name="hashMap">The hash map.</param>
        /// <param name="minIndicesPerJobCount">Min indices per job count.</param>
        /// <param name="dependsOn">The job handle dependency.</param>
        /// <typeparam name="TJob">The type of the job.</typeparam>
        /// <typeparam name="TKey">The type of the key in the hash map.</typeparam>
        /// <typeparam name="TValue">The type of the value in the hash map.</typeparam>
        /// <returns>The handle to job.</returns>
        public static unsafe JobHandle Schedule<TJob, TKey, TValue>(
            this TJob jobData,
            NativeMultiHashMap<TKey, TValue> hashMap,
            int minIndicesPerJobCount,
            JobHandle dependsOn = default)
            where TJob : struct, IJobNativeMultiHashMapVisitUniqueKeys<TKey, TValue>
            where TKey : struct, IEquatable<TKey>
            where TValue : struct
        {
            var imposter = (NativeMultiHashMapImposter<TKey, TValue>)hashMap;

            var fullData = new NativeMultiHashMapVisitUniqueKeyJobStruct<TJob, TKey, TValue>
            {
                HashMap = imposter,
                JobData = jobData,
            };

            var scheduleParams = new JobsUtility.JobScheduleParameters(
                UnsafeUtility.AddressOf(ref fullData),
                NativeMultiHashMapVisitUniqueKeyJobStruct<TJob, TKey, TValue>.Initialize(),
                dependsOn,
                ScheduleMode.Batched);

            return JobsUtility.ScheduleParallelFor(
                ref scheduleParams,
                imposter.Buffer->BucketCapacityMask + 1,
                minIndicesPerJobCount);
        }

        /// <summary>
        /// The job execution struct;
        /// </summary>
        /// <typeparam name="TJob">The type of the job.</typeparam>
        /// <typeparam name="TKey">The type of the key in the hash map.</typeparam>
        /// <typeparam name="TValue">The type of the value in the hash map.</typeparam>
        internal struct NativeMultiHashMapVisitUniqueKeyJobStruct<TJob, TKey, TValue>
            where TJob : struct, IJobNativeMultiHashMapVisitUniqueKeys<TKey, TValue>
            where TKey : struct, IEquatable<TKey>
            where TValue : struct
        {
            [ReadOnly]
            public NativeMultiHashMapImposter<TKey, TValue> HashMap;

            public TJob JobData;

            // ReSharper disable once StaticMemberInGenericType
            private static IntPtr JobReflectionData;

            internal static IntPtr Initialize()
            {
                if (JobReflectionData == IntPtr.Zero)
                {
                    JobReflectionData = JobsUtility.CreateJobReflectionData(
                        typeof(NativeMultiHashMapVisitUniqueKeyJobStruct<TJob, TKey, TValue>),
                        typeof(TJob),
                        JobType.ParallelFor,
                        (ExecuteJobFunction)Execute);
                }

                return JobReflectionData;
            }

            private delegate void ExecuteJobFunction(
                ref NativeMultiHashMapVisitUniqueKeyJobStruct<TJob, TKey, TValue> fullData,
                IntPtr additionalPtr,
                IntPtr bufferRangePatchData,
                ref JobRanges ranges,
                int jobIndex);

            // ReSharper disable once MemberCanBePrivate.Global - Required by Burst
            public static unsafe void Execute(
                ref NativeMultiHashMapVisitUniqueKeyJobStruct<TJob, TKey, TValue> fullData,
                IntPtr additionalPtr,
                IntPtr bufferRangePatchData,
                ref JobRanges ranges,
                int jobIndex)
            {
                while (true)
                {
                    if (!JobsUtility.GetWorkStealingRange(ref ranges, jobIndex, out var begin, out var end))
                    {
                        return;
                    }

                    var buckets = (int*)fullData.HashMap.Buffer->Buckets;
                    var keys = fullData.HashMap.Buffer->Keys;

                    for (int i = begin; i < end; i++)
                    {
                        int entryIndex = buckets[i];

                        if (entryIndex != -1)
                        {
                            var key = UnsafeUtility.ReadArrayElement<TKey>(keys, entryIndex);
                            fullData.JobData.Execute(key);
                        }
                    }
                }
            }
        }
    }
}