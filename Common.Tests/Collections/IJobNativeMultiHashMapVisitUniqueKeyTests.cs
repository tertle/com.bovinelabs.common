// <copyright file="IJobNativeMultiHashMapVisitUniqueKeyTests.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Tests.Collections
{
    using System.Collections.Generic;
    using BovineLabs.Common.Collections;
    using NUnit.Framework;
    using Unity.Burst;
    using Unity.Collections;

    /// <summary>
    /// The IJobNativeMultiHashMapVisitUniqueKeyTests.
    /// </summary>
    public class IJobNativeMultiHashMapVisitUniqueKeyTests
    {
        /// <summary>
        /// Tests scheduling a <see cref="IJobNativeMultiHashMapVisitUniqueKeys{TKey,TValue}"/>.
        /// </summary>
        [Test]
        public void Schedule()
        {
            var map = new NativeMultiHashMap<int, int>(128, Allocator.TempJob);

            map.Add(1, 1);
            map.Add(1, 2);
            map.Add(1, 3);

            map.Add(2, 1);
            map.Add(2, 2);

            map.Add(3, 1);

            var queue = new NativeQueue<int>(Allocator.TempJob);

            new TestJob { Output = queue.AsParallelWriter() }.Schedule(map, 4).Complete();

            Assert.AreEqual(3, queue.Count);

            var hashMap = new HashSet<int>();

            while (queue.TryDequeue(out var key))
            {
                Assert.IsTrue(key == 1 || key == 2 || key == 3);

                hashMap.Add(key);
            }

            Assert.AreEqual(3, hashMap.Count);

            queue.Dispose();
            map.Dispose();
        }

        [BurstCompile]
        private struct TestJob : IJobNativeMultiHashMapVisitUniqueKeys<int, int>
        {
            public NativeQueue<int>.ParallelWriter Output;

            public void Execute(int key)
            {
                this.Output.Enqueue(key);
            }
        }
    }
}