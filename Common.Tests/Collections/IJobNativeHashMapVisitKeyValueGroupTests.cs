// <copyright file="IJobNativeHashMapVisitKeyValueGroupTests.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Tests.Collections
{
    using BovineLabs.Common.Collections;
    using NUnit.Framework;
    using Unity.Burst;
    using Unity.Collections;

    /// <summary>
    /// The IJobNativeMultiHashMapVisitUniqueKeyTests.
    /// </summary>
    public class IJobNativeHashMapVisitKeyValueGroupTests
    {
        /// <summary>
        /// Tests scheduling a <see cref="IJobNativeHashMapVisitKeyValueGroup{TKey,TValue}"/>.
        /// </summary>
        [Test]
        public void Schedule()
        {
            var map = new NativeHashMap<int, int>(128, Allocator.TempJob);

            map.TryAdd(1, 1);
            map.TryAdd(2, 2);
            map.TryAdd(3, 3);
            map.TryAdd(4, 1);
            map.TryAdd(5, 2);
            map.Remove(1);
            map.TryAdd(6, 1);
            map.TryAdd(1, 1);

            var output = new NativeHashMap<int, int>(6, Allocator.TempJob);

            new TestJob { Output = output.AsParallelWriter() }.Schedule(map, 64).Complete();

            Assert.AreEqual(6, output.Length);

            Assert.IsTrue(output.TryGetValue(1, out var i));
            Assert.AreEqual(1, i);

            Assert.IsTrue(output.TryGetValue(2, out i));
            Assert.AreEqual(2, i);

            Assert.IsTrue(output.TryGetValue(3, out i));
            Assert.AreEqual(3, i);

            Assert.IsTrue(output.TryGetValue(4, out i));
            Assert.AreEqual(1, i);

            Assert.IsTrue(output.TryGetValue(5, out i));
            Assert.AreEqual(2, i);

            Assert.IsTrue(output.TryGetValue(6, out i));
            Assert.AreEqual(1, i);

            output.Dispose();
            map.Dispose();
        }

        [TestCase(5, 16)]
        [TestCase(9, 16)]
        [TestCase(16, 16)]
        [TestCase(33, 128)]
        [TestCase(67, 128)]
        [TestCase(128, 128)]
        [TestCase(123, 1024)]
        [TestCase(623, 1024)]
        [TestCase(1024, 1024)]
        [TestCase(12345, 65536)]
        [TestCase(55323, 65536)]
        [TestCase(65536, 65536)]
        public void CapacityTest(int length, int capacity)
        {
            var map = new NativeHashMap<int, int>(capacity, Allocator.TempJob);

            for (var i = 0; i < length; i++)
            {
                map.TryAdd(i, i);
            }

            var output = new NativeHashMap<int, int>(length, Allocator.TempJob);

            new TestJob { Output = output.AsParallelWriter() }.Schedule(map, 64).Complete();

            Assert.AreEqual(length, output.Length);

            output.Dispose();
            map.Dispose();
        }

        [BurstCompile]
        private struct TestJob : IJobNativeHashMapVisitKeyValueGroup<int, int>
        {
            public NativeHashMap<int, int>.ParallelWriter Output;

            public void Execute(NativeSlice<int> keys, NativeSlice<int> values)
            {
                for (var i = 0; i < keys.Length; i++)
                {
                    this.Output.TryAdd(keys[i], values[i]);
                }
            }
        }
    }
}