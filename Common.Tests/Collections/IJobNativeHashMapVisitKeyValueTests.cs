// <copyright file="IJobNativeHashMapVisitKeyValueTests.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Tests.Collections
{
    using BovineLabs.Common.Collections;
    using NUnit.Framework;
    using Unity.Burst;
    using Unity.Collections;

    /// <summary>
    /// The IJobNativeMultiHashMapVisitUniqueKeyTests.
    /// </summary>
    public class IJobNativeHashMapVisitKeyValueTests
    {
        /// <summary>
        /// Tests scheduling a <see cref="IJobNativeHashMapVisitKeyValue{TKey,TValue}"/>.
        /// </summary>
        [Test]
        public void Schedule()
        {
            var map = new NativeHashMap<int, int>(128, Allocator.TempJob);

            map.TryAdd(1, 1);
            map.TryAdd(2, 2);
            map.TryAdd(3, 3);
            map.TryAdd(4, 1);
            map.TryAdd(5, 2);
            map.Remove(1);
            map.TryAdd(6, 1);
            map.TryAdd(1, 1);

            var output = new NativeHashMap<int, int>(6, Allocator.TempJob);

            new TestJob { Output = output.AsParallelWriter() }.Schedule(map, 4).Complete();

            Assert.AreEqual(6, output.Length);

            Assert.IsTrue(output.TryGetValue(1, out var i));
            Assert.AreEqual(1, i);

            Assert.IsTrue(output.TryGetValue(2, out i));
            Assert.AreEqual(2, i);

            Assert.IsTrue(output.TryGetValue(3, out i));
            Assert.AreEqual(3, i);

            Assert.IsTrue(output.TryGetValue(4, out i));
            Assert.AreEqual(1, i);

            Assert.IsTrue(output.TryGetValue(5, out i));
            Assert.AreEqual(2, i);

            Assert.IsTrue(output.TryGetValue(6, out i));
            Assert.AreEqual(1, i);

            output.Dispose();
            map.Dispose();
        }

        [BurstCompile]
        private struct TestJob : IJobNativeHashMapVisitKeyValue<int, int>
        {
            public NativeHashMap<int, int>.ParallelWriter Output;

            public void ExecuteNext(int key, int value)
            {
                this.Output.TryAdd(key, value);
            }
        }
    }
}