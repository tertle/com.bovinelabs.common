// <copyright file="LocalToWorldExtensionsTests.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Tests.Extensions
{
    using BovineLabs.Common.Extensions;
    using NUnit.Framework;
    using Unity.Mathematics;
    using Unity.Transforms;
    using UnityEngine;

    /// <summary>
    /// Tests for <see cref="LocalToWorldExtensions"/>.
    /// </summary>
    public class LocalToWorldExtensionsTests
    {
        /// <summary>
        /// Tests the Rotation extension.
        /// </summary>
        [Test]
        public void Rotation()
        {
            var scale = new float3(12, 33, 123);
            var e = new float3(0.1f, 0.2f, 0.3f);

            var position = new float3(0, 0, 0);
            var rotation = quaternion.EulerXYZ(e);

            var localToWorld = new LocalToWorld
            {
                // seems to produce same result
                // Value = float4x4.TRS(position, rotation, scale),
                // But this is what Transform system uses so replicate
                Value = math.mul(new float4x4(rotation, position), float4x4.Scale(scale)),
            };

            var result = localToWorld.Rotation();

            AssertMath.AreApproximatelyEqual(rotation, result, 0.00001f);
        }
    }
}