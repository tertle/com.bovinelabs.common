// <copyright file="NativeMultiHashMapExtensionsTests.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Tests.Extensions
{
    using System.Collections.Generic;
    using BovineLabs.Common.Extensions;
    using NUnit.Framework;
    using Unity.Collections;

    /// <summary>
    /// The NativeMultiHashMapExtensionsTests.
    /// </summary>
    public class NativeMultiHashMapExtensionsTests
    {
        /// <summary>
        /// Tests the GetIterator extension.
        /// </summary>
        [Test]
        public void GetIterator()
        {
            using (var hashMap = new NativeMultiHashMap<int, int>(128, Allocator.TempJob))
            {
                hashMap.Add(1, 1);
                hashMap.Add(2, 1);
                hashMap.Add(2, 2);
                hashMap.Add(3, 3);
                hashMap.Add(1, 4);

                var it = hashMap.GetIterator();

                var keys = new Dictionary<int, int> { { 1, 0 }, { 2, 0 }, { 3, 0 } };
                var values = new Dictionary<int, int> { { 1, 0 }, { 2, 0 }, { 3, 0 }, { 4, 0 } };

                while (it.MoveNext())
                {
                    keys[it.Key] += 1;
                    values[it.Value] += 1;
                }

                Assert.AreEqual(values[1], 2);
                Assert.AreEqual(values[2], 1);
                Assert.AreEqual(values[3], 1);
                Assert.AreEqual(values[4], 1);

                Assert.AreEqual(keys[1], 2);
                Assert.AreEqual(keys[2], 2);
                Assert.AreEqual(keys[3], 1);
            }
        }

        /// <summary>
        /// Tests the clone extension.
        /// </summary>
        [Test]
        public void Clone()
        {
            using (var hashMap = new NativeMultiHashMap<int, int>(128, Allocator.TempJob))
            {
                hashMap.Add(1, 1);
                hashMap.Add(2, 1);
                hashMap.Add(2, 2);
                hashMap.Add(3, 3);
                hashMap.Add(1, 4);

                using (var clone = hashMap.Clone(Allocator.TempJob))
                {
                    Assert.AreEqual(hashMap.Length, clone.Length);
                    Assert.AreEqual(hashMap.Capacity, clone.Capacity);

                    Assert.IsTrue(hashMap.TryGetFirstValue(1, out var value, out var it));

                    // conveniently order will be maintained until remove
                    Assert.IsTrue(value == 1 || value == 4);

                    Assert.IsTrue(hashMap.TryGetNextValue(out var newValue, ref it));

                    Assert.AreNotEqual(value, newValue);
                    Assert.IsTrue(newValue == 1 || newValue == 4);

                    Assert.IsFalse(hashMap.TryGetNextValue(out newValue, ref it));
                }
            }
        }
    }
}