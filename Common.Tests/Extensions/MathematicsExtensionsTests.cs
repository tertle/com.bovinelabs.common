// <copyright file="MathematicsExtensionsTests.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Tests.Extensions
{
    using BovineLabs.Common.Extensions;
    using NUnit.Framework;
    using Unity.Mathematics;
    using Assert = UnityEngine.Assertions.Assert;

    /// <summary>
    /// The MathematicsExtensionsTests.
    /// </summary>
    public class MathematicsExtensionsTests
    {
        /// <summary>
        /// Tests ToEuler.
        /// </summary>
        [Test]
        public void ToEuler()
        {
            var e = new float3(0.1f, 0.2f, 0.3f);
            quaternion q = quaternion.Euler(e, math.RotationOrder.XYZ);
            var r = q.ToEuler();

            Assert.AreApproximatelyEqual(e.x, r.x);
            Assert.AreApproximatelyEqual(e.y, r.y);
            Assert.AreApproximatelyEqual(e.z, r.z);
        }
    }
}