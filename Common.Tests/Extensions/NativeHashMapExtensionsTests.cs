// <copyright file="NativeHashMapExtensionsTests.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Tests.Extensions
{
    using BovineLabs.Common.Extensions;
    using NUnit.Framework;
    using Unity.Collections;

    /// <summary>
    /// Tests for NativeHashMapExtensions.
    /// </summary>
    public class NativeHashMapExtensionsTests
    {
        /// <summary>
        /// Tests the GetIterator extension.
        /// </summary>
        [Test]
        public void GetIterator()
        {
            using (var hashMap = new NativeHashMap<int, int>(128, Allocator.TempJob))
            {
                hashMap.TryAdd(1, 1);
                hashMap.TryAdd(2, 1);
                hashMap.TryAdd(3, 2);
                hashMap.TryAdd(4, 3);
                hashMap.TryAdd(5, 4);

                using (var clone = hashMap.Clone(Allocator.TempJob))
                {
                    var it = hashMap.GetIterator();

                    while (it.MoveNext())
                    {
                        Assert.IsTrue(hashMap.TryGetValue(it.Key, out var val));
                        Assert.AreEqual(val, it.Value);
                        clone.Remove(it.Key);
                    }

                    Assert.AreEqual(0, clone.Length);
                }
            }
        }

        /// <summary>
        /// Tests the clone extension.
        /// </summary>
        [Test]
        public void Clone()
        {
            using (var hashMap = new NativeHashMap<int, int>(128, Allocator.TempJob))
            {
                hashMap.TryAdd(1, 1);
                hashMap.TryAdd(2, 1);
                hashMap.TryAdd(3, 2);

                using (var clone = hashMap.Clone(Allocator.TempJob))
                {
                    Assert.AreEqual(hashMap.Length, clone.Length);
                    Assert.AreEqual(hashMap.Capacity, clone.Capacity);

                    Assert.IsTrue(hashMap.TryGetValue(1, out var value));
                    Assert.AreEqual(1, value);

                    Assert.IsTrue(hashMap.TryGetValue(2, out value));
                    Assert.AreEqual(1, value);

                    Assert.IsTrue(hashMap.TryGetValue(3, out value));
                    Assert.AreEqual(2, value);
                }
            }
        }
    }
}