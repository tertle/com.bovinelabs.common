// <copyright file="NativeMultiHashMapTests.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Tests.Misc
{
    using NUnit.Framework;
    using Unity.Burst;
    using Unity.Collections;
    using Unity.Collections.LowLevel.Unsafe;
    using UnityEngine;

    /// <summary>
    /// Tests for NativeMultiHashMap.
    /// </summary>
    public class NativeMultiHashMapTests
    {
        /// <summary>
        /// Test to check if keys are unique to threads.
        /// </summary>
        /// <param name="keys">The number of keys to create.</param>
        [TestCase(1)]
        [TestCase(10)]
        [TestCase(1000)]
        [Test]
        public void CheckThreadSafety(int keys)
        {
            int count = 10000; // tested this at 10,000,000 still passed

            var data = new NativeMultiHashMap<int, byte>(count, Allocator.Persistent);

            for (var i = 0; i < count; i++)
            {
                data.Add(Random.Range(0, keys), (byte)Random.Range(0, 5));
            }

            var output = new NativeMultiHashMap<int, int>(count, Allocator.Persistent);

            new JobTest
                {
                    ThreadIndex = 0, // this just removes a warning from within Unity as it is implicitly set
                    Output = output.AsParallelWriter(),
                }
                .Schedule(data, 1).Complete();

            for (var i = 0; i < keys; i++)
            {
                if (!output.TryGetFirstValue(i, out var threadIndex, out var it))
                {
                    continue;
                }

                // Debug.Log(threadIndex); // quick visual test to make sure we're actually running on multiple threads
                while (output.TryGetNextValue(out var index, ref it))
                {
                    Assert.AreEqual(threadIndex, index);
                }
            }

            data.Dispose();
            output.Dispose();
        }

        [BurstCompile]
        private struct JobTest : IJobNativeMultiHashMapVisitKeyValue<int, byte>
        {
            [NativeSetThreadIndex]
            public int ThreadIndex;

            public NativeMultiHashMap<int, int>.ParallelWriter Output;

            public void ExecuteNext(int key, byte value)
            {
                this.Output.Add(key, this.ThreadIndex);

                // Busy work to make sure we use multiple threads
                FindPrimeNumber(0); // use something larger actual work
            }

            private static void FindPrimeNumber(int n)
            {
                int count = 0;
                long a = 2;
                while (count < n)
                {
                    long b = 2;
                    int prime = 1; // to check if found a prime
                    while (b * b <= a)
                    {
                        if (a % b == 0)
                        {
                            prime = 0;
                            break;
                        }

                        b++;
                    }

                    if (prime > 0)
                    {
                        count++;
                    }

                    a++;
                }
            }
        }
    }
}