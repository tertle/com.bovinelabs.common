// <copyright file="IntersectionsTest.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Tests.Helpers
{
    using BovineLabs.Common.Helpers;
    using NUnit.Framework;
    using Unity.Mathematics;

    /// <summary>
    /// Tests for <see cref="Intersections"/>.
    /// </summary>
    public class IntersectionsTest
    {
        /// <summary>
        /// Tests <see cref="Intersections.SegmentCircle"/>.
        /// </summary>
        /// <param name="x1">The start x.</param>
        /// <param name="y1">The start y.</param>
        /// <param name="x2">The end x.</param>
        /// <param name="y2">The end y.</param>
        /// <param name="c1">The center x.</param>
        /// <param name="c2">The center y.</param>
        /// <param name="radius">The radius.</param>
        /// <param name="result">The expected result.</param>
        [TestCase(-5, 0, 5, 0, 0, 0, 2, true)] // horizontal 2
        [TestCase(0, -5, 0, 5, 0, 0, 2, true)] // vertical 2
        [TestCase(-5, 0, 0, 0, 0, 0, 2, true)] // horizontal 1
        [TestCase(0, -5, 0, 0, 0, 0, 2, true)] // vertical 1
        [TestCase(0, 0, 5, 0, 0, 0, 2, true)] // horizontal 1 out
        [TestCase(0, 0, 0, 5, 0, 0, 2, true)] // vertical 1 out
        [TestCase(-5, 2, 5, 2, 0, 0, 2, true)] // touch
        [TestCase(-5, 3, 5, 3, 0, 0, 2, false)] // miss
        [TestCase(-2, 0, 2, 0, 0, 0, 4, true)] // inside
        [TestCase(0, 2, 2, 0, 0, 0, 1.5f, true)] // diagonal 2
        public void SegmentCircle(float x1, float y1, float x2, float y2, float c1, float c2, float radius, bool result)
        {
            var start = new float2(x1, y1);
            var end = new float2(x2, y2);
            var center = new float2(c1, c2);

            Assert.AreEqual(result, Intersections.SegmentCircle(start, end, center, radius));
        }
    }
}