// <copyright file="AssertMath.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Tests
{
    using NUnit.Framework;
    using Unity.Mathematics;

    /// <summary>
    /// The AssertExtensions.
    /// </summary>
    public static class AssertMath
    {
        /// <summary>
        /// Compares two quaternions to check if they are equal within delta range.
        /// </summary>
        /// <param name="expected">The expected result.</param>
        /// <param name="result">The actual result.</param>
        /// <param name="delta">Delta.</param>
        public static void AreApproximatelyEqual(quaternion expected, quaternion result, float delta)
        {
            Assert.AreEqual(expected.value.x, result.value.x, delta);
            Assert.AreEqual(expected.value.y, result.value.y, delta);
            Assert.AreEqual(expected.value.z, result.value.z, delta);
            Assert.AreEqual(expected.value.w, result.value.w, delta);
        }

        /// <summary>
        /// Compares two float3 to check if they are equal within delta range.
        /// </summary>
        /// <param name="expected">The expected result.</param>
        /// <param name="result">The actual result.</param>
        /// <param name="delta">Delta.</param>
        public static void AreApproximatelyEqual(float3 expected, float3 result, float delta)
        {
            Assert.AreEqual(expected.x, result.x, delta);
            Assert.AreEqual(expected.y, result.y, delta);
            Assert.AreEqual(expected.z, result.z, delta);
        }
    }
}