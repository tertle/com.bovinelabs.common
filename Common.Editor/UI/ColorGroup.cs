// <copyright file="ColorGroup.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Editor.UI
{
    using System;
    using UnityEngine;

    /// <summary>
    /// The ColorGroup.
    /// </summary>
    public class ColorGroup : IDisposable
    {
        private readonly Color previousColor;

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorGroup"/> class.
        /// </summary>
        /// <param name="color">The color.</param>
        public ColorGroup(Color color)
        {
            this.previousColor = GUI.color;
            GUI.color = color;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            GUI.color = this.previousColor;
        }
    }
}