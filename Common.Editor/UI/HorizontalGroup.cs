// <copyright file="HorizontalGroup.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Editor.UI
{
    using System;
    using UnityEditor;

    /// <summary>
    /// The DisabledGroup.
    /// </summary>
    public class HorizontalGroup : IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HorizontalGroup"/> class.
        /// </summary>
        public HorizontalGroup()
        {
            EditorGUILayout.BeginHorizontal();
        }

        /// <inheritdoc />
        public void Dispose()
        {
            EditorGUILayout.EndHorizontal();
        }
    }
}