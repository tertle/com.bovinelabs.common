// <copyright file="DisabledGroup.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Editor.UI
{
    using System;
    using UnityEditor;

    /// <summary>
    /// The DisabledGroup.
    /// </summary>
    public class DisabledGroup : IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DisabledGroup"/> class.
        /// </summary>
        /// <param name="disabled">If the group should be disabled.</param>
        public DisabledGroup(bool disabled)
        {
            EditorGUI.BeginDisabledGroup(disabled);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            EditorGUI.EndDisabledGroup();
        }
    }
}