// <copyright file="GUIUtil.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Editor.UI
{
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    /// <summary>
    /// The GUIUtil.
    /// </summary>
    public static class GUIUtil
    {
        private static readonly Dictionary<GUIStyle, GUIStyle> ActiveStyles = new Dictionary<GUIStyle, GUIStyle>();

        /// <summary>
        /// Draw a line.
        /// </summary>
        public static void Line()
        {
            EditorGUILayout.LabelField(GUIContent.none, GUI.skin.horizontalSlider);
        }

        /// <summary>
        /// Create an indent group.
        /// </summary>
        /// <param name="indent">Indent level.</param>
        /// <returns>The indent group.</returns>
        public static IndentGroup Indent(int indent = 1)
        {
            return new IndentGroup(indent);
        }

        /// <summary>
        /// Create an disabled group.
        /// </summary>
        /// <param name="disabled">If it should be disabled.</param>
        /// <returns>The disabled group.</returns>
        public static DisabledGroup Disable(bool disabled)
        {
            return new DisabledGroup(disabled);
        }

        /// <summary>
        /// Create an horizontal group.
        /// </summary>
        /// <returns>The horizontal group.</returns>
        public static HorizontalGroup Horizontal()
        {
            return new HorizontalGroup();
        }

        /// <summary>
        /// Create an color group.
        /// </summary>
        /// <param name="color">The color.</param>
        /// <returns>The color group.</returns>
        public static ColorGroup Color(Color color)
        {
            return new ColorGroup(color);
        }

        /// <summary>
        /// Convert a style to be active.
        /// </summary>
        /// <param name="style">The base style.</param>
        /// <returns>The active style.</returns>
        public static GUIStyle GetActiveStyle(GUIStyle style)
        {
            if (!ActiveStyles.TryGetValue(style, out var activeStyle))
            {
                activeStyle = new GUIStyle(style);
                activeStyle.normal.textColor = activeStyle.active.textColor;
                activeStyle.normal.background = activeStyle.active.background;
                ActiveStyles.Add(style, activeStyle);
            }

            return activeStyle;
        }
    }
}