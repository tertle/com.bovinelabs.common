// <copyright file="EditorHelper.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Editor.UI
{
    using System;
    using System.Reflection;
    using UnityEditor;

    /// <summary>
    /// The EditorHelper.
    /// </summary>
    public static class EditorHelper
    {
        /// <summary>
        /// Gets the object the property represents. https://github.com/lordofduct/spacepuppy-unity-framework-3.0.
        /// </summary>
        /// <param name="prop">The property.</param>
        /// <returns>The object.</returns>
        public static object GetTargetObjectOfProperty(SerializedProperty prop)
        {
            var path = prop.propertyPath.Replace(".Array.data[", "[");
            object obj = prop.serializedObject.targetObject;
            var elements = path.Split('.');
            foreach (var element in elements)
            {
                if (element.Contains("["))
                {
                    var elementName = element.Substring(0, element.IndexOf("[", StringComparison.Ordinal));
                    var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[", StringComparison.Ordinal))
                        .Replace("[", string.Empty).Replace("]", string.Empty));
                    obj = GetValue_Imp(obj, elementName, index);
                }
                else
                {
                    obj = GetValue_Imp(obj, element);
                }
            }

            return obj;
        }

        private static object GetValue_Imp(object source, string name)
        {
            if (source == null)
            {
                return null;
            }

            var type = source.GetType();

            while (type != null)
            {
                var f = GetField(type, name);
                if (f != null)
                {
                    return f.GetValue(source);
                }

                var p = GetProperty(type, name);
                if (p != null)
                {
                    return p.GetValue(source, null);
                }

                type = type.BaseType;
            }

            return null;
        }

        private static object GetValue_Imp(object source, string name, int index)
        {
            var enumerable = GetValue_Imp(source, name) as System.Collections.IEnumerable;
            if (enumerable == null)
            {
                return null;
            }

            var enm = enumerable.GetEnumerator();

            for (int i = 0; i <= index; i++)
            {
                if (!enm.MoveNext())
                {
                    return null;
                }
            }

            return enm.Current;
        }

        private static FieldInfo GetField(Type t, string name)
        {
            while (true)
            {
                if (t == null)
                {
                    return null;
                }

                var fieldInfo = t.GetField(name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly);
                if (fieldInfo != null)
                {
                    return fieldInfo;
                }

                t = t.BaseType;
            }
        }

        private static PropertyInfo GetProperty(Type t, string name)
        {
            while (true)
            {
                if (t == null)
                {
                    return null;
                }

                var propertyInfo = t.GetProperty(name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                if (propertyInfo != null)
                {
                    return propertyInfo;
                }

                t = t.BaseType;
            }
        }
    }
}