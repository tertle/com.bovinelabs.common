// <copyright file="IndentGroup.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Editor.UI
{
    using System;
    using UnityEditor;

    /// <summary>
    /// The IndentGroup.
    /// </summary>
    public class IndentGroup : IDisposable
    {
        private readonly int indent;

        /// <summary>
        /// Initializes a new instance of the <see cref="IndentGroup"/> class.
        /// </summary>
        /// <param name="indent">How far to indent.</param>
        public IndentGroup(int indent = 1)
        {
            this.indent = indent;

            EditorGUI.indentLevel += indent;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            EditorGUI.indentLevel -= this.indent;
        }
    }
}