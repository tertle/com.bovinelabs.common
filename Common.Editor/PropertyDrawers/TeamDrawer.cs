// <copyright file="TeamDrawer.cs" company="BovineLabs">
//     Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Editor.PropertyDrawers
{
    using BovineLabs.Common.Components;
    using UnityEditor;
    using UnityEngine;

    /// <summary>
    /// The ComponentEditor.
    /// </summary>
    [CustomPropertyDrawer(typeof(Team))]
    public class TeamDrawer : PropertyDrawer
    {
        private static readonly GUIContent TeamLabel = EditorGUIUtility.TrTextContent(
            "Team",
            "The team the observer provides vision to.");

        /// <inheritdoc />
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.PropertyField(position, property.FindPropertyRelative("Value"), TeamLabel);
        }
    }
}