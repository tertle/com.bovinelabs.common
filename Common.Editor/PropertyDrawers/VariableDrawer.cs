// <copyright file="VariableDrawer.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Editor.PropertyDrawers
{
    using System;
    using BovineLabs.Common.Variables;
    using UnityEditor;
    using UnityEngine;

    /// <summary>
    /// Base property drawer for <see cref="Variable{TR,T}"/>.
    /// </summary>
    [CustomPropertyDrawer(typeof(ColorVariable))]
    [CustomPropertyDrawer(typeof(BoolVariable))]
    [CustomPropertyDrawer(typeof(FloatVariable))]
    [CustomPropertyDrawer(typeof(IntVariable))]
    [CustomPropertyDrawer(typeof(LayerMaskVariable))]
    [CustomPropertyDrawer(typeof(RectVariable))]
    [CustomPropertyDrawer(typeof(StringVariable))]
    [CustomPropertyDrawer(typeof(Vector2Variable))]
    [CustomPropertyDrawer(typeof(Vector3Variable))]
    [CustomPropertyDrawer(typeof(Vector4Variable))]
    [CustomPropertyDrawer(typeof(Vector2IntVariable))]
    [CustomPropertyDrawer(typeof(Vector3IntVariable))]
    public class VariableDrawer : PropertyDrawer
    {
        private const float ToggleWidth = 64;
        private const float ToggleWidthSmall = 20;
        private static readonly GUIContent UseRef = new GUIContent("->Ref");
        private static readonly GUIContent UseConst = new GUIContent("->Const");
        private static readonly GUIContent ButtonSmall = new GUIContent("T");
        private static readonly GUIContent WidthWarning = new GUIContent("Inspector too small");

        /// <summary>
        /// Draw the VariableDrawer.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="property">The property.</param>
        /// <param name="label">The label.</param>
        /// <param name="drawProperty">The property drawing override.</param>
        public static void DrawGUI(Rect position, SerializedProperty property, GUIContent label, Action<Rect, SerializedProperty, GUIContent> drawProperty = null)
        {
            if (position.width < 313)
            {
                EditorGUI.LabelField(position, label, WidthWarning);
                return;
            }

            var useConstant = property.FindPropertyRelative("useConstant");

            var toggleWidth = ToggleWidth;
            var buttonText = useConstant.boolValue ? UseRef : UseConst;

            if (position.width < 350)
            {
                toggleWidth = ToggleWidthSmall;
                buttonText = ButtonSmall;
            }

            var propertyRect = new Rect(position.x, position.y, position.width - toggleWidth, position.height);
            var constRect = new Rect(position.x + position.width - toggleWidth, position.y, toggleWidth, position.height);

            var field = useConstant.boolValue
                ? property.FindPropertyRelative("constant")
                : property.FindPropertyRelative("variable");

            if (drawProperty != null)
            {
                drawProperty(propertyRect, field, label);
            }
            else
            {
                EditorGUI.PropertyField(propertyRect, field, label);
            }

            if (GUI.Button(constRect, buttonText))
            {
                useConstant.boolValue = !useConstant.boolValue;
            }
        }

        /// <inheritdoc />
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawGUI(position, property, label);
        }
    }
}