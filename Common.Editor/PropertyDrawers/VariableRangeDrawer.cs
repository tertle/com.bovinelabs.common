// <copyright file="VariableRangeDrawer.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Editor.PropertyDrawers
{
    using System;
    using BovineLabs.Common.Editor.UI;
    using BovineLabs.Common.Variables;
    using UnityEditor;
    using UnityEngine;

    /// <summary>
    /// The VariableRangeDrawer.
    /// </summary>
    [CustomPropertyDrawer(typeof(VariableRangeAttribute))]
    public class VariableRangeDrawer : PropertyDrawer
    {
        /// <inheritdoc />
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var target = EditorHelper.GetTargetObjectOfProperty(property);

            if (IsGenericTypeOf(typeof(Variable<,>), target.GetType()))
            {
                var useConstant = property.FindPropertyRelative("useConstant");

                Action<Rect, SerializedProperty, GUIContent> propertyDraw = null;

                if (useConstant.boolValue)
                {
                    switch (target)
                    {
                        case IntVariable _:
                            propertyDraw = this.DrawIntVariable;
                            break;
                        case FloatVariable _:
                            propertyDraw = this.DrawFloatVariable;
                            break;
                        default:
                            EditorGUI.LabelField(position, label.text, $"Use VariableRange with Int or Float variables");
                            return;
                    }
                }

                VariableDrawer.DrawGUI(position, property, label, propertyDraw);
            }
            else
            {
                EditorGUI.LabelField(position, label.text, $"Use VariableRange with {typeof(Variable<,>).Name}.");
            }
        }

        private static bool IsGenericTypeOf(Type genericType, Type someType)
        {
            if (someType.IsGenericType && genericType == someType.GetGenericTypeDefinition())
            {
                return true;
            }

            return someType.BaseType != null && IsGenericTypeOf(genericType, someType.BaseType);
        }

        private void DrawIntVariable(Rect rect, SerializedProperty property, GUIContent label)
        {
            var range = (VariableRangeAttribute)this.attribute;

            property.intValue = EditorGUI.IntSlider(
                rect,
                label,
                property.intValue,
                Convert.ToInt32(range.Min),
                Convert.ToInt32(range.Max));
        }

        private void DrawFloatVariable(Rect rect, SerializedProperty property, GUIContent label)
        {
            var range = (VariableRangeAttribute)this.attribute;

            property.floatValue = EditorGUI.Slider(
                rect,
                label,
                property.floatValue,
                range.Min,
                range.Max);
        }
    }
}