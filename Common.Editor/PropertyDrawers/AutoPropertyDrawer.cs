// <copyright file="AutoPropertyDrawer.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Common.Editor.PropertyDrawers
{
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    /// <summary>
    /// Property drawer that auto populates and sizes height. Used mostly for hiding foldouts.
    /// </summary>
    public abstract class AutoPropertyDrawer : PropertyDrawer
    {
        private const float Height = 15;

        private int autoIndex = 0;

        private List<string> properties;

        /// <inheritdoc />
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            // I think GetPropertyHeight should always be called before OnGUI
            if (this.properties == null)
            {
                this.properties = new List<string>();
            }
            else
            {
                this.properties.Clear();
            }

            this.GetVisible(property, this.properties);

            var sum = 0f;

            foreach (var p in this.properties)
            {
                sum += EditorGUI.GetPropertyHeight(property.FindPropertyRelative(p));
            }

            return sum;
        }

        /// <inheritdoc />
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            this.autoIndex = 0;

            for (var i = 0; i < this.properties.Count; i++)
            {
                this.PropertyField(position, property, this.properties[i]);
            }
        }

        /// <summary>
        /// Get all the visible properties.
        /// </summary>
        /// <param name="property">The parent property.</param>
        /// <param name="properties">The array to add the visible properties to.</param>
        protected abstract void GetVisible(SerializedProperty property, List<string> properties);

        private void PropertyField(Rect position, SerializedProperty property, string relativeProperty)
        {
            var rect = this.GetRect(position, this.autoIndex++);
            var prop = property.FindPropertyRelative(relativeProperty);
            EditorGUI.PropertyField(rect, prop);
        }

        private Rect GetRect(Rect position, int offset)
        {
            return new Rect(position.x, position.y + (offset * Height), position.width, Height);
        }
    }
}